<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Combination extends CI_Controller {

	// Load Database
	public function __construct()
	{
		parent::__construct();
		$this->load->model('combination_model');
		$this->load->model('kategori_jer_model');

	}

	// Listing data combination
	public function index()
	{
		$site 					= $this->konfigurasi_model->listing();
		$listing_kategori_jer	= $this->combination_model->listing_kategori_jer();

		// Ambil data total_combination 
		$total				= $this->combination_model->total_combination();

		// Paginasi Start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'combination/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_numbers']	= TRUE;
		$config['per_page'] 		= 3;
		$config['uri_segment']		= 3;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['first_link']		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link']	 	= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close']	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['firs_url']			= base_url().'/combination/';
		
		$this->pagination->initialize($config);

		// Ambil data combination
		$page 	= ($this->uri->segment(3)) ? ($this->uri->segment(3)-1) * $config['per_page']:0;
		$combination = $this->combination_model->normal($config['per_page'],$page);

		// Paginasi End

		$data 	= array(	'title'					=> 'Produk Kulit Kombinasi ',
							'site'					=> $site,
							'listing_kategori_jer'	=> $listing_kategori_jer,
							'combination'			=> $combination,
							'pagin'					=> $this->pagination->create_links(),
							'isi'					=> 'combination/list'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

	

	// Listing Kategori combination
	public function kategori_jer($slug_kategori)
	{
		// Kategori detail
		$kategori_jer 			= $this->kategori_jer_model->read($slug_kategori);
		$id_kategori 		= $kategori_jer->id_kategori;

		// Data Global
		$site 				= $this->konfigurasi_model->listing();
		$listing_kategori_jer	= $this->combination_model->listing_kategori_jer();

		// Ambil data total_combination 
		$total				= $this->combination_model->total_kategori_jer($id_kategori);

		// Paginasi Start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'combination/kategori_jer/'.$slug_kategori.'/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_numbers']	= TRUE;
		$config['per_page'] 		= 3;
		$config['uri_segment']		= 5;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['first_link']		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link']	 	= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close']	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['firs_url']			= base_url().'/combination/kategori_jer/'.$slug_kategori;
		
		$this->pagination->initialize($config);

		// Ambil data combination
		$page 	= ($this->uri->segment(5)) ? ($this->uri->segment(5)-1) * $config['per_page']:0;
		$combination = $this->combination_model->kategori_jer($id_kategori, $config['per_page'],$page);

		// Paginasi End

		$data 	= array(	'title'				=> $kategori_jer->nama_kategori,
							'site'				=> $site,
							'listing_kategori_jer'	=> $listing_kategori_jer,
							'combination'			=> $combination,
							'pagin'				=> $this->pagination->create_links(),
							'isi'				=> 'combination/list'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

	// Detail Combination
	public function detail($slug_produk)
	{
		$site 	 			= $this->konfigurasi_model->listing();
		$combination 			= $this->combination_model->read($slug_produk);
		$id_produk			= $combination->id_produk;
		$gambar_jer 			= $this->combination_model->gambar($id_produk);
		$produk_related	= $this->combination_model->home(); 	

		$data 	= array(	'title'				=> $combination->nama_produk,
							'site'				=> $site,
							'combination'		=> $combination,
							'produk_related'	=> $produk_related,
							'gambar_jer'		=> $gambar_jer,
							'isi'				=> 'combination/detail'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
	}

}

/* End of file Combination.php */
/* Location: ./application/controllers/Combination.php */