<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oil extends CI_Controller {

	//load model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oil_model');
		$this->load->model('kategori_oil_model');
		//proteksi halaman
		$this->simple_login->cek_login();
	}

	//data oil
	public function index()
	{
		$oil = $this->oil_model->listing();

		$data = array(	'title'			=> 'Data Produk Kulit Berminyak',
						'oil'		=> $oil,
						'isi'			=> 'admin/oil/list'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		
	}

	// Gambar
	public function gambar_oil($id_produk)
	{
		$oil 	= $this->oil_model->detail($id_produk);
		$gambar_oil 	= $this->oil_model->gambar($id_produk);


		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('judul_gambar','Judul/Nama Gambar','required',
			array(	'required'			=> '%s harus diisi'));


		if($valid->run()) {
			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2024';
			$config['max_height']  = '2024';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
			//end validasi

		$data = array(	'title'			=> 'Tambah Gambar Produk Kulit Berminyak'.$oil->nama_produk,
						'oil'	=> $oil,
						'gambar_oil' 	=> $gambar_oil,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/oil/gambar_oil'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
	
		$data = array(	'id_produk' 			=>$id_produk,
						'judul_gambar'			=> $i->post('judul_gambar'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar'				=> $upload_gambar['upload_data']['file_name']
	);
		$this->oil_model->tambah_gambar($data);
		$this->session->set_flashdata('sukses', 'Data gambar_oil telah ditambah');
		redirect(base_url('admin/oil/gambar_oil/'.$id_produk),'refresh');
	}}
	// End masuk database
	$data = array(		'title'			=> 'Tambah Gambar Produk Kulit Berminyak : '.$oil->nama_produk,
						'oil'	=> $oil,
						'gambar_oil' 	=> $gambar_oil,
						'isi'			=> 'admin/oil/gambar_oil'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);

	}


		//Tambah Oil
	public function tambah()
	{
		// Ambil data kategori_oil
		$kategori_oil = $this->kategori_oil_model->listing();

		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('nama_produk','Nama produk','required',
			array(	'required'			=> '%s harus diisi'));

		$valid->set_rules('kode_produk','Kode produk','required|is_unique[oil.kode_produk]',
			array(	'required'			=> '%s harus diisi',

					'is_unique'			=> '%s sudah ada. Buat kode produk baru'));


		if($valid->run()) {
			$config['upload_path'] 		= './assets/upload/image/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['max_size']  		= '2400'; //dalan KB
			$config['max_width']  		= '2024';
			$config['max_height']  		= '2024';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
			//end validasi

		$data = array(	'title'			=> 'Tambah Produk Kulit Berminyak',
						'kategori_oil'	=> $kategori_oil,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/oil/tambah'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);
		$data = array(	'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk'),
						'tanggal_post'			=> date('Y-m-d H:i:s')
	);
		$this->oil_model->tambah($data);
		$this->session->set_flashdata('sukses', 'Data telah ditambah');
		redirect(base_url('admin/oil'),'refresh');
	}}
	// End masuk database
	$data = array(		'title'			=> 'Tambah Produk Kulit Berminyak',
						'kategori_oil'		=> $kategori_oil,
						'isi'			=> 'admin/oil/tambah'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);


	}


		//Edit Oil
	public function edit($id_produk)
	{
		// Ambil data oil yang diedit
		$oil 	= $this->oil_model->detail($id_produk);
		// Ambil data kategori_oil
		$kategori_oil 	= $this->kategori_oil_model->listing();

		// validasi input
		$valid 		= $this->form_validation;

		$valid->set_rules('nama_produk','Nama oil','required',
			array(	'required'			=> '%s harus diisi'));

		$valid->set_rules('kode_produk','Kode oil','required',
			array(	'required'			=> '%s harus diisi'));


		if($valid->run()) {
			// Check jika gambar_oil diganti
			if(!empty($_FILES['gambar_oil']['name'])) {

			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2024';
			$config['max_height']  = '2024';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar_oil')){
				
			//end validasi

		$data = array(	'title'			=> 'Edit Produk Kulit Berminyak: '.$oil->nama_produk,
						'kategori_oil'		=> $kategori_oil,
						'oil'		=> $oil,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/oil/edit/'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);

		$data = array(	'id_produk'				=> $id_produk,
						'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar_oil'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk')
	);
		$this->oil_model->edit($data);
		$this->session->set_flashdata('sukses', 'Data telah diedit');
		redirect(base_url('admin/oil'),'refresh');
	}}else{
		// EDIT PRODUK TANPA GANTI GAMBAR
		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);

		$data = array(	'id_produk'				=> $id_produk,
						'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR(GAMBAR TDK DIGANTI)
						//'gambar_oil'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk')
	);
		$this->oil_model->edit($data);
		$this->session->set_flashdata('sukses', 'Data telah diedit');
		redirect(base_url('admin/oil'),'refresh');

	}}
	// End masuk database
	$data = array(		'title'			=> 'Edit Produk Kulit Berminyak: ' .$oil->nama_produk,
						'kategori_oil'		=> $kategori_oil,
						'oil'		=> $oil,
						'isi'			=> 'admin/oil/edit'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);



	}



	// Delete oil
	public function delete($id_produk)
	{
		// Proses hapus gambar_oil
		$oil = $this->oil_model->detail($id_produk);
		unlink('./assets/upload/image/'.$oil->gambar);
		unlink('./assets/upload/image/thumbs/'.$oil->gambar);
		// End proses hapus
		$data = array('id_produk' => $id_produk);
		$this->oil_model->delete($data);
		$this->session->set_flashdata('sukses', 'Data telah dihapus');
		redirect(base_url('admin/oil'),'refresh');
	}

		// Delete gambar_oil oil oil
	public function delete_gambar_oil($id_produk,$id_gambar)
	{
		// Proses hapus gambar_oil
		$gambar_oil = $this->oil_model->detail_gambar_oil($id_gambar);
		unlink('./assets/upload/image/'.$gambar_oil->gambar);
		unlink('./assets/upload/image/thumbs/'.$gambar_oil->gambar);
		// End proses hapus
		$data = array('id_gambar' => $id_gambar);
		$this->oil_model->delete_gambar_oil($data);
		$this->session->set_flashdata('sukses', 'Data gambar_oil telah dihapus');
		redirect(base_url('admin/oil/gambar_oil/'.$id_produk),'refresh');
	}

}

/* End of file Oil.php */
/* Location: ./application/controllers/admin/Oil.php */