<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Normal extends CI_Controller {

	//load model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('normal_model');
		$this->load->model('kategori_model');
		//proteksi halaman
		$this->simple_login->cek_login();
	}

	//data normal
	public function index()
	{
		$normal = $this->normal_model->listing();

		$data = array(	'title'			=> 'Data Normal Kulit Normal',
						'normal'		=> $normal,
						'isi'			=> 'admin/normal/list'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		
	}

	// Gambar
	public function gambar($id_produk)
	{
		$normal 	= $this->normal_model->detail($id_produk);
		$gambar 	= $this->normal_model->gambar($id_produk);


		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('judul_gambar','Judul/Nama Gambar','required',
			array(	'required'			=> '%s harus diisi'));


		if($valid->run()) {
			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2024';
			$config['max_height']  = '2024';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
			//end validasi

		$data = array(	'title'			=> 'Tambah Gambar Normal Kulit Normal'.$normal->nama_produk,
						'normal'		=> $normal,
						'gambar' 		=> $gambar,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/normal/gambar'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
	
		$data = array(	'id_produk' 			=>$id_produk,
						'judul_gambar'			=> $i->post('judul_gambar'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar'				=> $upload_gambar['upload_data']['file_name']
	);
		$this->normal_model->tambah_gambar($data);
		$this->session->set_flashdata('sukses', 'Data gambar telah ditambah');
		redirect(base_url('admin/normal/gambar/'.$id_produk),'refresh');
	}}
	// End masuk database
	$data = array(		'title'			=> 'Tambah Gambar Normal Kulit Normal : '.$normal->nama_produk,
						'normal'		=> $normal,
						'gambar' 		=> $gambar,
						'isi'			=> 'admin/normal/gambar'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);

	}


		//Tambah Normal
	public function tambah()
	{
		// Ambil data kategori
		$kategori = $this->kategori_model->listing();

		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('nama_produk','Nama Produk','required',
			array(	'required'			=> '%s harus diisi'));

		$valid->set_rules('kode_produk','Kode Produk','required|is_unique[normal.kode_produk]',
			array(	'required'			=> '%s harus diisi',

					'is_unique'			=> '%s sudah ada. Buat kode normal baru'));


		if($valid->run()) {
			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2400';
			$config['max_height']  = '2400';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
			//end validasi

		$data = array(	'title'			=> 'Tambah Normal Kulit Normal',
						'kategori'		=> $kategori,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/normal/tambah'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);
		$data = array(	'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk'),
						'tanggal_post'			=> date('Y-m-d H:i:s')
	);
		$this->normal_model->tambah($data); 
		$this->session->set_flashdata('sukses', 'Data telah ditambah');
		redirect(base_url('admin/normal'),'refresh');
	}}
	// End masuk database
	$data = array(		'title'			=> 'Tambah Normal Kulit Normal',
						'kategori'		=> $kategori,
						'isi'			=> 'admin/normal/tambah'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);


	}


		//Edit Normal
	public function edit($id_produk)
	{
		// Ambil data normal yang diedit
		$normal 	= $this->normal_model->detail($id_produk);
		// Ambil data kategori
		$kategori 	= $this->kategori_model->listing();

		// validasi input
		$valid 		= $this->form_validation;

		$valid->set_rules('nama_produk','Nama normal','required',
			array(	'required'			=> '%s harus diisi'));

		$valid->set_rules('kode_produk','Kode normal','required',
			array(	'required'			=> '%s harus diisi'));


		if($valid->run()) {
			// Check jika gambar diganti
			if(!empty($_FILES['gambar']['name'])) {

			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2400';
			$config['max_height']  = '2400';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
			//end validasi

		$data = array(	'title'			=> 'Edit Normal Kulit Normal: '.$normal->nama_produk,
						'kategori'		=> $kategori,
						'normal'		=> $normal,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/normal/edit'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);

		$data = array(	'id_produk'				=> $id_produk,
						'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk')
	);
		$this->normal_model->edit($data);
		$this->session->set_flashdata('sukses', 'Data telah diedit');
		redirect(base_url('admin/normal'),'refresh');
	}}else{
		// EDIT PRODUK TANPA GANTI GAMBAR
		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);

		$data = array(	'id_produk'				=> $id_produk,
						'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR(GAMBAR TDK DIGANTI)
						//'gambar'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk')
	);
		$this->normal_model->edit($data);
		$this->session->set_flashdata('sukses', 'Data telah diedit');
		redirect(base_url('admin/normal'),'refresh');

	}}
	// End masuk database
	$data = array(		'title'			=> 'Edit Normal Kulit Normal: ' .$normal->nama_produk,
						'kategori'		=> $kategori,
						'normal'		=> $normal,
						'isi'			=> 'admin/normal/edit'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);



	}



	// Delete normal
	public function delete($id_produk)
	{
		// Proses hapus gambar
		$normal = $this->normal_model->detail($id_produk);
		unlink('./assets/upload/image/'.$normal->gambar);
		unlink('./assets/upload/image/thumbs/'.$normal->gambar);
		// End proses hapus
		$data = array('id_produk' => $id_produk);
		$this->normal_model->delete($data);
		$this->session->set_flashdata('sukses', 'Data telah dihapus');
		redirect(base_url('admin/normal'),'refresh');
	}

		// Delete gambar normal normal
	public function delete_gambar($id_produk,$id_gambar)
	{
		// Proses hapus gambar
		$gambar = $this->normal_model->detail_gambar($id_gambar);
		unlink('./assets/upload/image/'.$gambar->gambar);
		unlink('./assets/upload/image/thumbs/'.$gambar->gambar);
		// End proses hapus
		$data = array('id_gambar' => $id_gambar);
		$this->normal_model->delete_gambar($data);
		$this->session->set_flashdata('sukses', 'Data gambar telah dihapus');
		redirect(base_url('admin/normal/gambar/'.$id_produk),'refresh');
	}

}

/* End of file Normal.php */
/* Location: ./application/controllers/admin/Normal.php */