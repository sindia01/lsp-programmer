<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Combination extends CI_Controller {

	//load model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('combination_model');
		$this->load->model('kategori_jer_model');
		//proteksi halaman
		$this->simple_login->cek_login();
	}

	//data combination
	public function index()
	{
		$combination = $this->combination_model->listing();

		$data = array(	'title'			=> 'Data Produk Kulit Kombinasi',
						'combination'	=> $combination,
						'isi'			=> 'admin/combination/list'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		
	}

	// Gambar
	public function gambar_jer($id_produk)
	{
		$combination 	= $this->combination_model->detail($id_produk);
		$gambar_jer 	= $this->combination_model->gambar($id_produk);


		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('judul_gambar','Judul/Nama Gambar','required',
			array(	'required'			=> '%s harus diisi'));


		if($valid->run()) {
			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2024';
			$config['max_height']  = '2024';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
			//end validasi

		$data = array(	'title'			=> 'Tambah Gambar Produk Kulit Kombinasi'.$combination->nama_produk,
						'combination'		=> $combination,
						'gambar_jer' 		=> $gambar_jer,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/combination/gambar_jer'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
	
		$data = array(	'id_produk' 			=>$id_produk,
						'judul_gambar'			=> $i->post('judul_gambar'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar'				=> $upload_gambar['upload_data']['file_name']
	);
		$this->combination_model->tambah_gambar($data);
		$this->session->set_flashdata('sukses', 'Data gambar_jer telah ditambah');
		redirect(base_url('admin/combination/gambar_jer/'.$id_produk),'refresh');
	}}
	// End masuk database
	$data = array(		'title'			=> 'Tambah Gambar Produk Kulit Kombinasi : '.$combination->nama_produk,
						'combination'		=> $combination,
						'gambar_jer' 		=> $gambar_jer,
						'isi'			=> 'admin/combination/gambar_jer'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);

	}


		//Tambah Combination
	public function tambah()
	{
		// Ambil data kategori_jer
		$kategori_jer = $this->kategori_jer_model->listing();

		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('nama_produk','Nama produk','required',
			array(	'required'			=> '%s harus diisi'));

		$valid->set_rules('kode_produk','Kode produk','required|is_unique[combination.kode_produk]',
			array(	'required'			=> '%s harus diisi',

					'is_unique'			=> '%s sudah ada. Buat kode produk baru'));


		if($valid->run()) {
			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2024';
			$config['max_height']  = '2024';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
			//end validasi

		$data = array(	'title'			=> 'Tambah Produk Kulit Kombinasi',
						'kategori_jer'	=> $kategori_jer,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/combination/tambah'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);
		$data = array(	'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk'),
						'tanggal_post'			=> date('Y-m-d H:i:s')
	);
		$this->combination_model->tambah($data);
		$this->session->set_flashdata('sukses', 'Data telah ditambah');
		redirect(base_url('admin/combination'),'refresh');
	}}
	// End masuk database
	$data = array(		'title'			=> 'Tambah Produk Kulit Kombinasi',
						'kategori_jer'		=> $kategori_jer,
						'isi'			=> 'admin/combination/tambah'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);


	}


		//Edit Combination
public function edit($id_produk)
{
// Ambil data combination yang diedit
$combination 	= $this->combination_model->detail($id_produk);
// Ambil data kategori_jer
$kategori_jer 	= $this->kategori_jer_model->listing();

// validasi input
$valid 		= $this->form_validation;

$valid->set_rules('nama_produk','Nama combination','required',
array(	'required'			=> '%s harus diisi'));

$valid->set_rules('kode_produk','Kode combination','required',
array(	'required'			=> '%s harus diisi'));


if($valid->run()) {
// Check jika gambar_jer diganti
if(!empty($_FILES['gambar_jer']['name'])) {

$config['upload_path'] = './assets/upload/image/';
$config['allowed_types'] = 'gif|jpg|png|jpeg';
$config['max_size']  = '2400'; //dalan KB
$config['max_width']  = '2024';
$config['max_height']  = '2024';

$this->load->library('upload', $config);

if ( ! $this->upload->do_upload('gambar_jer')){

//end validasi

$data = array(	'title'			=> 'Edit Produk Kulit Kombinasi: '.$combination->nama_produk,
		'kategori_jer'	=> $kategori_jer,
		'combination'	=> $combination,
		'error'			=> $this->upload->display_errors(),
		'isi'			=> 'admin/combination/edit/'

	);
$this->load->view('admin/layout/wrapper', $data, FALSE);
// Masuk Database
}else{
$upload_gambar = array('upload_data' => $this->upload->data());

// CREATE THUMBNAIL GAMBAR

$config['image_library']	= 'gd2';
$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
// LOKASI FOLDER THUMBNAIL
$config['new_image']		='./assets/upload/image/thumbs/';
$config['create_thumb'] 	= TRUE;
$config['maintain_ratio'] 	= TRUE;
$config['width']         	= 250;//pixel
$config['height']       	= 250;//pixel
$config['thumb_marker']		='';

$this->load->library('image_lib', $config);

$this->image_lib->resize();
// END CREATE THUMBHNAIL


$i = $this->input;
// SLUG PRODUK
$slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);

$data = array(	'id_produk'				=> $id_produk,
		'id_user'				=> $this->session->userdata('id_user'),
		'id_kategori'			=> $i->post('id_kategori'),							
		'merk '					=> $i->post('merk'),
		'kode_produk'			=> $i->post('kode_produk'),
		'nama_produk'			=> $i->post('nama_produk'),
		'slug_produk'			=> $slug_produk,
		'keterangan'			=> $i->post('keterangan'),
		'keyword'				=> $i->post('keyword'),
		'harga'					=> $i->post('harga'),
			// YANG DI SAVE NAMA FILE GAMBAR
		'gambar_jer'				=> $upload_gambar['upload_data']['file_name'],
		'status_produk'			=> $i->post('status_produk')
);
$this->combination_model->edit($data);
$this->session->set_flashdata('sukses', 'Data telah diedit');
redirect(base_url('admin/combination'),'refresh');
}}else{
// EDIT PRODUK TANPA GANTI GAMBAR
$i = $this->input;
// SLUG PRODUK
$slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);

$data = array(	'id_produk'				=> $id_produk,
		'id_user'				=> $this->session->userdata('id_user'),
		'id_kategori'			=> $i->post('id_kategori'),
		'merk '					=> $i->post('merk'),
		'kode_produk'			=> $i->post('kode_produk'),
		'nama_produk'			=> $i->post('nama_produk'),
		'slug_produk'			=> $slug_produk,
		'keterangan'			=> $i->post('keterangan'),
		'keyword'				=> $i->post('keyword'),
		'harga'					=> $i->post('harga'),
			// YANG DI SAVE NAMA FILE GAMBAR(GAMBAR TDK DIGANTI)
		//'gambar_jer'				=> $upload_gambar['upload_data']['file_name'],
		'status_produk'			=> $i->post('status_produk')
);
$this->combination_model->edit($data);
$this->session->set_flashdata('sukses', 'Data telah diedit');
redirect(base_url('admin/combination'),'refresh');

}}
// End masuk database
$data = array(		'title'			=> 'Edit Produk Kulit Kombinasi: ' .$combination->nama_produk,
		'kategori_jer'	=> $kategori_jer,
		'combination'	=> $combination,
		'isi'			=> 'admin/combination/edit'

	);
$this->load->view('admin/layout/wrapper', $data, FALSE);



}



	// Delete combination
	public function delete($id_produk)
	{
		// Proses hapus gambar_jerzzzz
		$combination = $this->combination_model->detail($id_produk);
		unlink('./assets/upload/image/'.$combination->gambar);
		unlink('./assets/upload/image/thumbs/'.$combination->gambar);
		// End proses hapus
		$data = array('id_produk' => $id_produk);
		$this->combination_model->delete($data);
		$this->session->set_flashdata('sukses', 'Data telah dihapus');
		redirect(base_url('admin/combination'),'refresh');
	}

		// Delete gambar_jer combination combination
	public function delete_gambar_jer($id_produk,$id_gambar) 
	{
		// Proses hapus gambar_jer
		$gambar_jer = $this->combination_model->detail_gambar_jer($id_gambar);
		unlink('./assets/upload/image/'.$gambar_jer->gambar);
		unlink('./assets/upload/image/thumbs/'.$gambar_jer->gambar);
		// End proses hapus
		$data = array('id_gambar' => $id_gambar);
		$this->combination_model->delete_gambar_jer($data);
		$this->session->set_flashdata('sukses', 'Data gambar telah dihapus');
		redirect(base_url('admin/combination/gambar_jer/'.$id_produk),'refresh');
	}

}

/* End of file Combination.php */
/* Location: ./application/controllers/admin/Combination.php */