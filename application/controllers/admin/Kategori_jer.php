<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_jer extends CI_Controller {

	//load model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kategori_jer_model');
		//proteksi halaman
		$this->simple_login->cek_login();
	}

	//data kategori
	public function index()
	{
		$kategori_jer = $this->kategori_jer_model->listing();

		$data = array(	'title'			=> 'Data Kategori Produk Jerawat',
						'kategori_jer'		=> $kategori_jer,
						'isi'			=> 'admin/kategori_jer/list'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		
	}


		//Tambah Kategori_jer
	public function tambah()
	{
		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('nama_kategori','Nama kategori','required|is_unique[kategori_jer.nama_kategori]',
			array(		  'required'			=> '%s harus diisi',
						  'is_unique'			=> '%s sudah ada. buat kategori baru!'));


		if($valid->run()===FALSE) {
			//end validasi

		$data = array(	'title'			=> 'Tambah Kategori_jer Produk',
						'isi'			=> 'admin/kategori_jer/tambah'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$i 				= $this->input;
		 $slug_kategori = url_title($this->input->post('nama_kategori'), 'dash', TRUE);


		$data = array(	'slug_kategori'	=> $slug_kategori,
						'nama_kategori'	=> $i->post('nama_kategori'),
						'urutan'		=> $i->post('urutan')
	);
		$this->kategori_jer_model->tambah($data);
		$this->session->set_flashdata('sukses', 'Data telah ditambah');
		redirect(base_url('admin/kategori_jer'),'refresh');
	}
	// End masuk database
	}


		//Edit Kategori_jer
	public function edit($id_kategori)
	{
		$kategori_jer= $this->kategori_jer_model->detail($id_kategori);


		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('nama_kategori','Nama kategori','required',
			array(	'required'			=> '%s harus diisi'));


		if($valid->run()===FALSE) {
			//end validasi

		$data = array(	'title'				=> 'Edit Kategori Produk Jerawat',
						'kategori_jer'		=> $kategori_jer,
						'isi'				=> 'admin/kategori_jer/edit'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$i 				= $this->input;
		$slug_kategori	= url_title($this->input->post('nama_kategori'), 'dash', TRUE);
		$data = array(	'id_kategori'		=> $id_kategori,
						'slug_kategori'		=> $slug_kategori,
						'nama_kategori'		=> $i->post('nama_kategori'),
						'urutan'			=> $i->post('urutan')
	);
		$this->kategori_jer_model->edit($data);
		$this->session->set_flashdata('sukses', 'Data telah diedit');
		redirect(base_url('admin/kategori_jer'),'refresh');
	}
	// End masuk database
	}

	// Delete kategori
	public function delete($id_kategori)
	{
		$data = array('id_kategori' => $id_kategori);
		$this->kategori_jer_model->delete($data);
		$this->session->set_flashdata('sukses', 'Data telah dihapus');
		redirect(base_url('admin/kategori_jer'),'refresh');
	}

}

/* End of file Kategori_jer.php */
/* Location: ./application/controllers/admin/Kategori_jer.php */