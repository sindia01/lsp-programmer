<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dry extends CI_Controller {

	//load model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('dry_model');
		$this->load->model('kategori_dry_model');
		//proteksi halaman
		$this->simple_login->cek_login();
	}

	//data dry
	public function index()
	{
		$dry = $this->dry_model->listing();

		$data = array(	'title'			=> 'Data Produk Kulit Kering',
						'dry'		=> $dry,
						'isi'			=> 'admin/dry/list'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		
	}

	// Gambar
	public function gambar_dry($id_produk)
	{
		$dry 	= $this->dry_model->detail($id_produk);
		$gambar_dry 	= $this->dry_model->gambar($id_produk);


		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('judul_gambar','Judul/Nama Gambar','required',
			array(	'required'			=> '%s harus diisi'));


		if($valid->run()) {
			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2024';
			$config['max_height']  = '2024';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
			//end validasi

		$data = array(	'title'			=> 'Tambah Gambar Produk Kulit Kering'.$dry->nama_produk,
						'dry'		=> $dry,
						'gambar_dry' 		=> $gambar_dry,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/dry/gambar_dry'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
	
		$data = array(	'id_produk' 			=>$id_produk,
						'judul_gambar'			=> $i->post('judul_gambar'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar'				=> $upload_gambar['upload_data']['file_name']
	);
		$this->dry_model->tambah_gambar($data);
		$this->session->set_flashdata('sukses', 'Data gambar_dry telah ditambah');
		redirect(base_url('admin/dry/gambar_dry/'.$id_produk),'refresh');
	}}
	// End masuk database
	$data = array(		'title'			=> 'Tambah Gambar Produk Kulit Kering : '.$dry->nama_produk,
						'dry'		=> $dry,
						'gambar_dry' 		=> $gambar_dry,
						'isi'			=> 'admin/dry/gambar_dry'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);

	}


		//Tambah Dry
	public function tambah()
	{
		// Ambil data kategori_dry
		$kategori_dry = $this->kategori_dry_model->listing();

		// validasi input
		$valid = $this->form_validation;

		$valid->set_rules('nama_produk','Nama produk','required',
			array(	'required'			=> '%s harus diisi'));

		$valid->set_rules('kode_produk','Kode produk','required|is_unique[dry.kode_produk]',
			array(	'required'			=> '%s harus diisi',

					'is_unique'			=> '%s sudah ada. Buat kode produk baru'));


		if($valid->run()) {
			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2024';
			$config['max_height']  = '2024';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar')){
				
			//end validasi

		$data = array(	'title'			=> 'Tambah Produk Kulit Kering',
						'kategori_dry'	=> $kategori_dry,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/dry/tambah'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);
		$data = array(	'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk'),
						'tanggal_post'			=> date('Y-m-d H:i:s')
	);
		$this->dry_model->tambah($data);
		$this->session->set_flashdata('sukses', 'Data telah ditambah');
		redirect(base_url('admin/dry'),'refresh');
	}}
	// End masuk database
	$data = array(		'title'			=> 'Tambah Produk Kulit Kering',
						'kategori_dry'		=> $kategori_dry,
						'isi'			=> 'admin/dry/tambah'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);


	}


		//Edit Dry
	public function edit($id_produk)
	{
		// Ambil data dry yang diedit
		$dry 	= $this->dry_model->detail($id_produk);
		// Ambil data kategori_dry
		$kategori_dry 	= $this->kategori_dry_model->listing();

		// validasi input
		$valid 		= $this->form_validation;

		$valid->set_rules('nama_produk','Nama dry','required',
			array(	'required'			=> '%s harus diisi'));

		$valid->set_rules('kode_produk','Kode dry','required',
			array(	'required'			=> '%s harus diisi'));


		if($valid->run()) {
			// Check jika gambar_dry diganti
			if(!empty($_FILES['gambar_dry']['name'])) {

			$config['upload_path'] = './assets/upload/image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '2400'; //dalan KB
			$config['max_width']  = '2024';
			$config['max_height']  = '2024';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('gambar_dry')){
				
			//end validasi

		$data = array(	'title'				=> 'Edit Produk Kulit Kering: '.$dry->nama_produk,
						'kategori_dry'		=> $kategori_dry,
						'dry'				=> $dry,
						'error'				=> $this->upload->display_errors(),
						'isi'				=> 'admin/dry/edit/'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		// Masuk Database
	}else{
		$upload_gambar = array('upload_data' => $this->upload->data());

		// CREATE THUMBNAIL GAMBAR

			$config['image_library']	= 'gd2';
			$config['source_image'] 	= './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
				// LOKASI FOLDER THUMBNAIL
			$config['new_image']		='./assets/upload/image/thumbs/';
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= 250;//pixel
			$config['height']       	= 250;//pixel
			$config['thumb_marker']		='';

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		// END CREATE THUMBHNAIL


		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);

		$data = array(	'id_produk'				=> $id_produk,
						'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR
						'gambar_dry'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk')
	);
		$this->dry_model->edit($data);
		$this->session->set_flashdata('sukses', 'Data telah diedit');
		redirect(base_url('admin/dry'),'refresh');
	}}else{
		// EDIT PRODUK TANPA GANTI GAMBAR
		$i = $this->input;
		// SLUG PRODUK
		   $slug_produk = url_title($this->input->post('nama_produk'). '-' .$this->input->post('kode_produk'), 'dash', TRUE);

		$data = array(	'id_produk'				=> $id_produk,
						'id_user'				=> $this->session->userdata('id_user'),
						'id_kategori'			=> $i->post('id_kategori'),
						'merk '					=> $i->post('merk'),
						'kode_produk'			=> $i->post('kode_produk'),
						'nama_produk'			=> $i->post('nama_produk'),
						'slug_produk'			=> $slug_produk,
						'keterangan'			=> $i->post('keterangan'),
						'keyword'				=> $i->post('keyword'),
						'harga'					=> $i->post('harga'),
							// YANG DI SAVE NAMA FILE GAMBAR(GAMBAR TDK DIGANTI)
						//'gambar_dry'				=> $upload_gambar['upload_data']['file_name'],
						'status_produk'			=> $i->post('status_produk')
	);
		$this->dry_model->edit($data);
		$this->session->set_flashdata('sukses', 'Data telah diedit');
		redirect(base_url('admin/dry'),'refresh');

	}}
	// End masuk database
	$data = array(		'title'				=> 'Edit Produk Kulit Kering: ' .$dry->nama_produk,
						'kategori_dry'		=> $kategori_dry,
						'dry'		=> $dry,
						'isi'				=> 'admin/dry/edit'

					);
		$this->load->view('admin/layout/wrapper', $data, FALSE);



	}

  

	// Delete dry
	public function delete($id_produk)
	{
		// Proses hapus gambar_dry
		$dry = $this->dry_model->detail($id_produk);
		unlink('./assets/upload/image/'.$dry->gambar);
		unlink('./assets/upload/image/thumbs/'.$dry->gambar);
		// End proses hapus
		$data = array('id_produk' => $id_produk);
		$this->dry_model->delete($data);
		$this->session->set_flashdata('sukses', 'Data telah dihapus');
		redirect(base_url('admin/dry'),'refresh');
	}

		// Delete gambar_dry dry dry
	public function delete_gambar_dry($id_produk,$id_gambar)
	{
		// Proses hapus gambar_dry
		$gambar_dry = $this->dry_model->detail_gambar_dry($id_gambar);
		unlink('./assets/upload/image/'.$gambar_dry->gambar);
		unlink('./assets/upload/image/thumbs/'.$gambar_dry->gambar);
		// End proses hapus
		$data = array('id_gambar' => $id_gambar);
		$this->dry_model->delete_gambar_dry($data);
		$this->session->set_flashdata('sukses', 'Data gambar telah dihapus');
		redirect(base_url('admin/dry/gambar_dry/'.$id_produk),'refresh');
	}

}

/* End of file Dry.php */
/* Location: ./application/controllers/admin/Dry.php */