<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Normal extends CI_Controller {

	// Load Database
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Normal_model');
		$this->load->model('kategori_model');

	}

	// Listing data normal
	public function index()
	{
		$site 				= $this->konfigurasi_model->listing();
		$listing_kategori	= $this->Normal_model->listing_kategori();

		// Ambil data total 
		$total 				= $this->Normal_model->total_normal();

		// Paginasi Start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'normal/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_numbers']	= TRUE;
		$config['per_page'] 		= 3;
		$config['uri_segment']		= 3;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['first_link']		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link']	 	= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close']	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['firs_url']			= base_url().'/normal/';
		
		$this->pagination->initialize($config);

		// Ambil data normal
		$page 	= ($this->uri->segment(3)) ? ($this->uri->segment(3)-1) * $config['per_page']:0;
		$normal = $this->Normal_model->normal($config['per_page'],$page);

		// Paginasi End

		$data 	= array(	'title'				=> 'Produk Kulit Normal ',
							'site'				=> $site,
							'listing_kategori'	=> $listing_kategori,
							'normal'			=> $normal,
							'pagin'				=> $this->pagination->create_links(),
							'isi'				=> 'normal/list'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

	// Listing Kategori normal
	public function kategori($slug_kategori)
	{
		// Kategori detail
		$kategori 			= $this->kategori_model->read($slug_kategori);
		$id_kategori 		= $kategori->id_kategori;

		// Data Global
		$site 				= $this->konfigurasi_model->listing();
		$listing_kategori	= $this->Normal_model->listing_kategori();

		// Ambil data total 
		$total 				= $this->Normal_model->total_kategori($id_kategori);

		// Paginasi Start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'normal/kategori/'.$slug_kategori.'/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_numbers']	= TRUE;
		$config['per_page'] 		= 3;
		$config['uri_segment']		= 5;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['first_link']		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link']	 	= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close']	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['firs_url']			= base_url().'/normal/kategori/'.$slug_kategori;
		
		$this->pagination->initialize($config);

		// Ambil data normal
		$page 	= ($this->uri->segment(5)) ? ($this->uri->segment(5)-1) * $config['per_page']:0;
		$normal = $this->Normal_model->kategori($id_kategori, $config['per_page'],$page);

		// Paginasi End

		$data 	= array(	'title'				=> $kategori->nama_kategori,
							'site'				=> $site,
							'listing_kategori'	=> $listing_kategori,
							'normal'			=> $normal,
							'pagin'				=> $this->pagination->create_links(),
							'isi'				=> 'normal/list'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

	// Detail Normal
	public function detail($slug_produk)
	{
		$site 	 			= $this->konfigurasi_model->listing();
		$normal 			= $this->Normal_model->read($slug_produk);
		$id_produk			= $normal->id_produk;
		$gambar 			= $this->Normal_model->gambar($id_produk);
		$normal_related		= $this->Normal_model->home(); 	

		$data 	= array(	'title'				=> $normal->nama_produk,
							'site'				=> $site,
							'normal'			=> $normal,
							'normal_related'	=> $normal_related,
							'gambar'			=> $gambar,
							'isi'				=> 'normal/detail'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
	}

}

/* End of file Normal.php */
/* Location: ./application/controllers/Normal.php */