<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oil extends CI_Controller {

	// Load Database
	public function __construct()
	{
		parent::__construct();
		$this->load->model('oil_model');
		$this->load->model('kategori_oil_model');

	}

	// Listing data oil
	public function index()
	{
		$site 					= $this->konfigurasi_model->listing();
		$listing_kategori_oil	= $this->oil_model->listing_kategori_oil();

		// Ambil data total_oil 
		$total				= $this->oil_model->total_oil();

		// Paginasi Start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'oil/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_numbers']	= TRUE;
		$config['per_page'] 		= 3;
		$config['uri_segment']		= 3;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['first_link']		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link']	 	= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close']	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['firs_url']			= base_url().'/oil/';
		
		$this->pagination->initialize($config);

		// Ambil data oil
		$page 	= ($this->uri->segment(3)) ? ($this->uri->segment(3)-1) * $config['per_page']:0;
		$oil = $this->oil_model->normal($config['per_page'],$page);

		// Paginasi End

		$data 	= array(	'title'					=> 'Produk Kulit Berminyak ',
							'site'					=> $site,
							'listing_kategori_oil'	=> $listing_kategori_oil,
							'oil'			=> $oil,
							'pagin'					=> $this->pagination->create_links(),
							'isi'					=> 'oil/list'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

	// Listing Kategori oil
	public function kategori_oil($slug_kategori)
	{
		// Kategori detail
		$kategori_oil 			= $this->kategori_oil_model->read($slug_kategori);
		$id_kategori 			= $kategori_oil->id_kategori;

		// Data Global
		$site 					= $this->konfigurasi_model->listing();
		$listing_kategori_oil	= $this->oil_model->listing_kategori_oil();

		// Ambil data total_oil 
		$total				= $this->oil_model->total_kategori_oil($id_kategori);

		// Paginasi Start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'oil/kategori_oil/'.$slug_kategori.'/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_numbers']	= TRUE;
		$config['per_page'] 		= 3;
		$config['uri_segment']		= 5;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['first_link']		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link']	 	= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close']	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['firs_url']			= base_url().'/oil/kategori_oil/'.$slug_kategori;
		
		$this->pagination->initialize($config);

		// Ambil data oil
		$page 	= ($this->uri->segment(5)) ? ($this->uri->segment(5)-1) * $config['per_page']:0;
		$oil = $this->oil_model->kategori_oil($id_kategori, $config['per_page'],$page);

		// Paginasi End

		$data 	= array(	'title'					=> $kategori_oil->nama_kategori,
							'site'					=> $site,
							'listing_kategori_oil'	=> $listing_kategori_oil,
							'oil'			=> $oil,
							'pagin'					=> $this->pagination->create_links(),
							'isi'					=> 'oil/list'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

	// Detail Oil
	public function detail($slug_produk)
	{
		$site 	 				= $this->konfigurasi_model->listing();
		$oil 			= $this->oil_model->read($slug_produk);
		$id_produk				= $oil->id_produk;
		$gambar_oil 			= $this->oil_model->gambar($id_produk);
		$produk_related			= $this->oil_model->home(); 	

		$data 	= array(	'title'				=> $oil->nama_produk,
							'site'				=> $site,
							'oil'		=> $oil,
							'produk_related'	=> $produk_related,
							'gambar_oil'		=> $gambar_oil,
							'isi'				=> 'oil/detail'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
	}

}

/* End of file Oil.php */
/* Location: ./application/controllers/Oil.php */