<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dry extends CI_Controller {

	// Load Database
	public function __construct()
	{
		parent::__construct();
		$this->load->model('dry_model');
		$this->load->model('kategori_dry_model');

	}

	// Listing data dry
	public function index()
	{
		$site 					= $this->konfigurasi_model->listing();
		$listing_kategori_dry	= $this->dry_model->listing_kategori_dry();

		// Ambil data total_dry 
		$total				= $this->dry_model->total_dry();

		// Paginasi Start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'dry/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_numbers']	= TRUE;
		$config['per_page'] 		= 3;
		$config['uri_segment']		= 3;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['first_link']		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link']	 	= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close']	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['firs_url']			= base_url().'/dry/';
		
		$this->pagination->initialize($config);

		// Ambil data dry
		$page 	= ($this->uri->segment(3)) ? ($this->uri->segment(3)-1) * $config['per_page']:0;
		$dry = $this->dry_model->normal($config['per_page'],$page);

		// Paginasi End

		$data 	= array(	'title'					=> 'Produk Kulit Kering ',
							'site'					=> $site,
							'listing_kategori_dry'	=> $listing_kategori_dry,
							'dry'					=> $dry,
							'pagin'					=> $this->pagination->create_links(),
							'isi'					=> 'dry/list'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

	// Listing Kategori dry
	public function kategori_dry($slug_kategori)
	{
		// Kategori detail
		$kategori_dry 			= $this->kategori_dry_model->read($slug_kategori);
		$id_kategori 			= $kategori_dry->id_kategori;

		// Data Global
		$site 					= $this->konfigurasi_model->listing();
		$listing_kategori_dry	= $this->dry_model->listing_kategori_dry();

		// Ambil data total_dry 
		$total				= $this->dry_model->total_kategori_dry($id_kategori);

		// Paginasi Start
		$this->load->library('pagination');
		
		$config['base_url'] 		= base_url().'dry/kategori_dry/'.$slug_kategori.'/index/';
		$config['total_rows'] 		= $total->total;
		$config['use_page_numbers']	= TRUE;
		$config['per_page'] 		= 3;
		$config['uri_segment']		= 5;
		$config['num_links'] 		= 5;
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['first_link']		= 'First';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last';
		$config['last_tag_open'] 	= '<li class="disabled"><li class="active"><a href="#">';
		$config['last_tag_close'] 	= '<span class="sr-only"></a></li></li>';
		$config['next_link']	 	= '&gt;';
		$config['next_tag_open'] 	= '<div>';
		$config['next_tag_close']	= '</div>';
		$config['prev_link'] 		= '&lt;';
		$config['prev_tag_open'] 	= '<div>';
		$config['prev_tag_close'] 	= '</div>';
		$config['cur_tag_open'] 	= '<b>';
		$config['cur_tag_close'] 	= '</b>';
		$config['firs_url']			= base_url().'/dry/kategori_dry/'.$slug_kategori;
		
		$this->pagination->initialize($config);

		// Ambil data dry
		$page 	= ($this->uri->segment(5)) ? ($this->uri->segment(5)-1) * $config['per_page']:0;
		$dry = $this->dry_model->kategori_dry($id_kategori, $config['per_page'],$page);

		// Paginasi End

		$data 	= array(	'title'					=> $kategori_dry->nama_kategori,
							'site'					=> $site,
							'listing_kategori_dry'	=> $listing_kategori_dry,
							'dry'			=> $dry,
							'pagin'					=> $this->pagination->create_links(),
							'isi'					=> 'dry/list'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

	// Detail Dry
	public function detail($slug_produk)
	{
		$site 	 				= $this->konfigurasi_model->listing();
		$dry 			= $this->dry_model->read($slug_produk);
		$id_produk				= $dry->id_produk;
		$gambar_dry 			= $this->dry_model->gambar($id_produk);
		$produk_related			= $this->dry_model->home(); 	

		$data 	= array(	'title'				=> $dry->nama_produk,
							'site'				=> $site,
							'dry'		=> $dry,
							'produk_related'	=> $produk_related,
							'gambar_dry'		=> $gambar_dry,
							'isi'				=> 'dry/detail'
		);
		$this->load->view('layout/wrapper', $data, FALSE);
	}

}

/* End of file Dry.php */
/* Location: ./application/controllers/Dry.php */