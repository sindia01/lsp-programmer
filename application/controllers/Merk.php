<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merk extends CI_Controller {

	// Load model  
	public function __construct()
	{
		parent::__construct();
		$this->load->model('normal_model');
		$this->load->model('combination_model');
		$this->load->model('oil_model');
		$this->load->model('dry_model');
		$this->load->model('kategori_model');
		$this->load->model('kategori_jer_model');
		$this->load->model('kategori_oil_model');
		$this->load->model('konfigurasi_model');
	}

	// halaman utama website - homepage
	public function index()
	{
		$site 				= $this->konfigurasi_model->listing();
		$kategori_jer 		= $this->konfigurasi_model->nav_combination();
		$kategori_oil 		= $this->konfigurasi_model->nav_oil();
		$kategori_dry 		= $this->konfigurasi_model->nav_dry();
		$kategori 			= $this->konfigurasi_model->nav_normal();
		$normal 			= $this->normal_model->home();
		//$produk_jer = $this->produk_jer_model->home();

		$data = array(	'title'			=> 'Merk '.$site->namaweb,
						'isi'			=> 'merk/list'
						);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}




}

/* End of file About.php */
/* Location: ./application/controllers/About.php */