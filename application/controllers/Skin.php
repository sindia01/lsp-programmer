<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skin extends CI_Controller {

	public function index()
	{
		$site 		= $this->konfigurasi_model->listing();
		//$normal_jer = $this->normal_jer_model->home();

		$data = array(	'title'			=> 'Kenali Tipe Kulitmu ',
						'isi'			=> 'skin/list'
						);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

	

		 public function os()
	 	{
		$site 		= $this->konfigurasi_model->listing();
		//$normal_jer = $this->normal_jer_model->home();

		$data = array(	'title'			=> 'Kulit Berminyak ',
						'isi'			=> 'skin/os'
						);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}
		public function cs()
		 	{
			$site 		= $this->konfigurasi_model->listing();
			//$normal_jer = $this->normal_jer_model->home();

			$data = array(	'title'			=> 'Kulit Kombinasi ',
							'isi'			=> 'skin/cs'
							);
			$this->load->view('layout/wrapper', $data, FALSE);
			
		}

			 public function ds()
	 	{
		$site 		= $this->konfigurasi_model->listing();
		//$normal_jer = $this->normal_jer_model->home();

		$data = array(	'title'			=> 'Kulit Kering',
						'isi'			=> 'skin/ds'
						);
		$this->load->view('layout/wrapper', $data, FALSE);
		 
	}

			 public function ns()
	 	{
		$site 		= $this->konfigurasi_model->listing();
				//$normal_jer = $this->normal_jer_model->home();

		$data = array(	'title'			=> 'Kulit Normal ',
						'isi'			=> 'skin/ns'
						);
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

}

/* End of file Skin.php */
/* Location: ./application/controllers/Skin.php */