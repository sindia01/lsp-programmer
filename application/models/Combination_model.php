<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Combination_model extends CI_Model {

	public function __construct()
		{
			parent::__construct();
			$this->load->database();
			
		}	

		// Listing all produk
		public function listing()
		{
			$this->db->select('combination.*,
							users.nama,
							kategori_jer.nama_kategori,
							kategori_jer.slug_kategori,
							COUNT(gambar_jer.id_gambar) AS total_gambar');
			$this->db->from('combination');
			// JOIN
			$this->db->join('users', 'users.id_user = combination.id_user', 'left');
			$this->db->join('kategori_jer', 'kategori_jer.id_kategori = combination.id_kategori', 'left');
			$this->db->join('gambar_jer', 'gambar_jer.id_produk = combination.id_produk', 'left');
			//END JOIN
			$this->db->group_by('combination.id_produk');
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->result();
		}

		// Listing all produk home
		public function home()
		{
			$this->db->select('combination.*,
							users.nama,
							kategori_jer.nama_kategori,
							kategori_jer.slug_kategori,
							COUNT(gambar_jer.id_gambar) AS total_gambar');
			$this->db->from('combination');
			// JOIN
			$this->db->join('users', 'users.id_user = combination.id_user', 'left');
			$this->db->join('kategori_jer', 'kategori_jer.id_kategori = combination.id_kategori', 'left');
			$this->db->join('gambar_jer', 'gambar_jer.id_produk = combination.id_produk', 'left');
			//END JOIN
			$this->db->where('combination.status_produk','Publish');
			$this->db->group_by('combination.id_produk');
			$this->db->order_by('id_produk',' desc');
			$this->db->limit(12);
			$query = $this->db->get();
			return $query->result();
		}

		//  read produk
		public function read($slug_produk)
		{
			$this->db->select('combination.*,
							users.nama,
							kategori_jer.nama_kategori,
							kategori_jer.slug_kategori,
							COUNT(gambar_jer.id_gambar) AS total_gambar');
			$this->db->from('combination');
			// JOIN
			$this->db->join('users', 'users.id_user = combination.id_user', 'left');
			$this->db->join('kategori_jer', 'kategori_jer.id_kategori = combination.id_kategori', 'left');
			$this->db->join('gambar_jer', 'gambar_jer.id_produk = combination.id_produk', 'left');
			//END JOIN
			$this->db->where('combination.status_produk','Publish');
			$this->db->where('combination.slug_produk', $slug_produk);
			$this->db->group_by('combination.id_produk');
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->row();
		}

		// Produk
		public function normal($limit,$start)
		{
			$this->db->select('combination.*,
							users.nama,
							kategori_jer.nama_kategori,
							kategori_jer.slug_kategori,
							COUNT(gambar_jer.id_gambar) AS total_gambar');
			$this->db->from('combination');
			// JOIN
			$this->db->join('users', 'users.id_user = combination.id_user', 'left');
			$this->db->join('kategori_jer', 'kategori_jer.id_kategori = combination.id_kategori', 'left');
			$this->db->join('gambar_jer', 'gambar_jer.id_produk = combination.id_produk', 'left');
			//END JOIN
			$this->db->where('combination.status_produk','Publish');
			$this->db->group_by('combination.id_produk');
			$this->db->order_by('id_produk',' desc');
			$this->db->limit($limit,$start);
			$query = $this->db->get();
			return $query->result();
		}

		// Total Produk
		public function total_combination()
		{
			$this->db->select('COUNT(*) AS total');
			$this->db->from('combination');
			$this->db->where('status_produk', 'Publish');
			$query = $this->db->get();
			return $query->row();
		}


		// Kategori Produk
		public function kategori_jer($id_kategori,$limit,$start)
		{
			$this->db->select('combination.*,
							users.nama,
							kategori_jer.nama_kategori,
							kategori_jer.slug_kategori,
							COUNT(gambar_jer.id_gambar) AS total_gambar');
			$this->db->from('combination');
			// JOIN
			$this->db->join('users', 'users.id_user = combination.id_user', 'left');
			$this->db->join('kategori_jer', 'kategori_jer.id_kategori = combination.id_kategori', 'left');
			$this->db->join('gambar_jer', 'gambar_jer.id_produk = combination.id_produk', 'left');
			//END JOIN
			$this->db->where('combination.status_produk','Publish');
			$this->db->where('combination.id_kategori', $id_kategori);
			$this->db->group_by('combination.id_produk');
			$this->db->order_by('id_produk',' desc');
			$this->db->limit($limit,$start);
			$query = $this->db->get();
			return $query->result();
		}

		// Total Kategori Produk
		public function total_kategori_jer($id_kategori)
		{
			$this->db->select('COUNT(*) AS total');
			$this->db->from('combination');
			$this->db->where('status_produk', 'Publish');
			$this->db->where('id_kategori', $id_kategori);
			$query = $this->db->get();
			return $query->row();
		}

		// Listing kategori_jer
		public function listing_kategori_jer()
		{
			$this->db->select('combination.*,
							users.nama,
							kategori_jer.nama_kategori,
							kategori_jer.slug_kategori,
							COUNT(gambar_jer.id_gambar) AS total_gambar');
			$this->db->from('combination');
			// JOIN
			$this->db->join('users', 'users.id_user = combination.id_user', 'left');
			$this->db->join('kategori_jer', 'kategori_jer.id_kategori = combination.id_kategori', 'left');
			$this->db->join('gambar_jer', 'gambar_jer.id_produk = combination.id_produk', 'left');
			//END JOIN
			$this->db->group_by('combination.id_kategori');
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->result();
		}

		// Detail produk
		public function detail($id_produk)
		{
			$this->db->select('*');
			$this->db->from('combination');
			$this->db->where('id_produk', $id_produk);
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->row();
		}

		// Detail gambar produk
		public function detail_gambar_jer($id_gambar)
		{
			$this->db->select('*');
			$this->db->from('gambar_jer');
			$this->db->where('id_gambar', $id_gambar);
			$this->db->order_by('id_gambar',' desc');
			$query = $this->db->get();
			return $query->row();
		}

		// Gambar
		public function gambar($id_produk)
		{
			$this->db->select('*');
			$this->db->from('gambar_jer');
			$this->db->where('id_produk', $id_produk);
			$this->db->order_by('id_gambar','desc');
			$query = $this->db->get();
			return $query->result();
		}


		//Tambah
		public function tambah($data)
		{
			$this->db->insert('combination', $data);
		}

		//Tambah Gambar
		public function tambah_gambar($data)
		{
			$this->db->insert('gambar_jer', $data);
		}

  
		//Edit
		public function edit($data)
		{
			$this->db->where('id_produk', $data['id_produk']);
			$this->db->update('combination', $data);
		}


		//Delete
		public function delete($data)
		{
			$this->db->where('id_produk', $data['id_produk']);
			$this->db->delete('combination', $data);
		}

		//Delete Gambar
		public function delete_gambar_jer($data)
		{
			$this->db->where('id_gambar', $data['id_gambar']);
			$this->db->delete('gambar_jer', $data);
		}


}

/* End of file Combination_model.php */
/* Location: ./application/models/Combination_model.php */