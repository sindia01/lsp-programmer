<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigurasi_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing
	public function listing()
	{
		$query = $this->db->get('konfigurasi');
		return $query->row();
	}

	// EDIT
	public function edit($data)
	{
		$this->db->where('id_konfigurasi', $data['id_konfigurasi']);
		$this->db->update('konfigurasi', $data);
	}



		// load kategori produk  Berjerawat
	public function nav_combination()
	{
					$this->db->select('combination.*,
							kategori_jer.nama_kategori,
							kategori_jer.slug_kategori');
			$this->db->from('combination');
			// JOIN
			$this->db->join('kategori_jer', 'kategori_jer.id_kategori = combination.id_kategori', 'left');
			//END JOIN
			$this->db->group_by('combination.id_kategori');
			$this->db->order_by('kategori_jer.urutan',' ASC');
			$query = $this->db->get();
			return $query->result();
	}
	

			// load kategori produk  Berminyak
	public function nav_oil()
	{
					$this->db->select('oil.*,
							kategori_oil.nama_kategori,
							kategori_oil.slug_kategori');
			$this->db->from('oil');
			// JOIN
			$this->db->join('kategori_oil', 'kategori_oil.id_kategori = oil.id_kategori', 'left');
			//END JOIN
			$this->db->group_by('oil.id_kategori');
			$this->db->order_by('kategori_oil.urutan',' ASC');
			$query = $this->db->get();
			return $query->result();
	}


	// load kategori produk  Berminyak
	public function nav_dry()
	{
					$this->db->select('dry.*,
							kategori_dry.nama_kategori,
							kategori_dry.slug_kategori');
			$this->db->from('dry');
			// JOIN
			$this->db->join('kategori_dry', 'kategori_dry.id_kategori = dry.id_kategori', 'left');
			//END JOIN
			$this->db->group_by('dry.id_kategori');
			$this->db->order_by('kategori_dry.urutan',' ASC');
			$query = $this->db->get();
			return $query->result();
	}


	// load kategori produk Nomal
	public function nav_normal()
	{
					$this->db->select('normal.*,
							kategori.nama_kategori,
							kategori.slug_kategori');
			$this->db->from('normal');
			// JOIN
			$this->db->join('kategori', 'kategori.id_kategori = normal.id_kategori', 'left');
			//END JOIN
			$this->db->group_by('normal.id_kategori');
			$this->db->order_by('kategori.urutan',' ASC');
			$query = $this->db->get();
			return $query->result();
	}


	

}

/* End of file Konfigurasi_model.php */
/* Location: ./application/models/Konfigurasi_model.php */