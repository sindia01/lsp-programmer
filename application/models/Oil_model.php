<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oil_model extends CI_Model {

	public function __construct()
		{
			parent::__construct();
			$this->load->database();
			
		}	

		// Listing all produk
		public function listing()
		{
			$this->db->select('oil.*,
							users.nama,
							kategori_oil.nama_kategori,
							kategori_oil.slug_kategori,
							COUNT(gambar_oil.id_gambar) AS total_gambar');
			$this->db->from('oil');
			// JOIN
			$this->db->join('users', 'users.id_user = oil.id_user', 'left');
			$this->db->join('kategori_oil', 'kategori_oil.id_kategori = oil.id_kategori', 'left');
			$this->db->join('gambar_oil', 'gambar_oil.id_produk = oil.id_produk', 'left');
			//END JOIN
			$this->db->group_by('oil.id_produk');
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->result();
		}

		// Listing all produk home
		public function home()
		{
			$this->db->select('oil.*,
							users.nama,
							kategori_oil.nama_kategori,
							kategori_oil.slug_kategori,
							COUNT(gambar_oil.id_gambar) AS total_gambar');
			$this->db->from('oil');
			// JOIN
			$this->db->join('users', 'users.id_user = oil.id_user', 'left');
			$this->db->join('kategori_oil', 'kategori_oil.id_kategori = oil.id_kategori', 'left');
			$this->db->join('gambar_oil', 'gambar_oil.id_produk = oil.id_produk', 'left');
			//END JOIN
			$this->db->where('oil.status_produk','Publish');
			$this->db->group_by('oil.id_produk');
			$this->db->order_by('id_produk',' desc');
			$this->db->limit(12);
			$query = $this->db->get();
			return $query->result();
		}

		//  read produk
		public function read($slug_produk)
		{
			$this->db->select('oil.*,
							users.nama,
							kategori_oil.nama_kategori,
							kategori_oil.slug_kategori,
							COUNT(gambar_oil.id_gambar) AS total_gambar');
			$this->db->from('oil');
			// JOIN
			$this->db->join('users', 'users.id_user = oil.id_user', 'left');
			$this->db->join('kategori_oil', 'kategori_oil.id_kategori = oil.id_kategori', 'left');
			$this->db->join('gambar_oil', 'gambar_oil.id_produk = oil.id_produk', 'left');
			//END JOIN
			$this->db->where('oil.status_produk','Publish');
			$this->db->where('oil.slug_produk', $slug_produk);
			$this->db->group_by('oil.id_produk');
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->row();
		}

		// Produk
		public function normal($limit,$start)
		{
			$this->db->select('oil.*,
							users.nama,
							kategori_oil.nama_kategori,
							kategori_oil.slug_kategori,
							COUNT(gambar_oil.id_gambar) AS total_gambar');
			$this->db->from('oil');
			// JOIN
			$this->db->join('users', 'users.id_user = oil.id_user', 'left');
			$this->db->join('kategori_oil', 'kategori_oil.id_kategori = oil.id_kategori', 'left');
			$this->db->join('gambar_oil', 'gambar_oil.id_produk = oil.id_produk', 'left');
			//END JOIN
			$this->db->where('oil.status_produk','Publish');
			$this->db->group_by('oil.id_produk');
			$this->db->order_by('id_produk',' desc');
			$this->db->limit($limit,$start);
			$query = $this->db->get();
			return $query->result();
		}

		// Total Produk
		public function total_oil()
		{
			$this->db->select('COUNT(*) AS total');
			$this->db->from('oil');
			$this->db->where('status_produk', 'Publish');
			$query = $this->db->get();
			return $query->row();
		}


		// Kategori Produk
		public function kategori_oil($id_kategori,$limit,$start)
		{
			$this->db->select('oil.*,
							users.nama,
							kategori_oil.nama_kategori,
							kategori_oil.slug_kategori,
							COUNT(gambar_oil.id_gambar) AS total_gambar');
			$this->db->from('oil');
			// JOIN
			$this->db->join('users', 'users.id_user = oil.id_user', 'left');
			$this->db->join('kategori_oil', 'kategori_oil.id_kategori = oil.id_kategori', 'left');
			$this->db->join('gambar_oil', 'gambar_oil.id_produk = oil.id_produk', 'left');
			//END JOIN
			$this->db->where('oil.status_produk','Publish');
			$this->db->where('oil.id_kategori', $id_kategori);
			$this->db->group_by('oil.id_produk');
			$this->db->order_by('id_produk',' desc');
			$this->db->limit($limit,$start);
			$query = $this->db->get();
			return $query->result();
		}

		// Total Kategori Produk
		public function total_kategori_oil($id_kategori)
		{
			$this->db->select('COUNT(*) AS total');
			$this->db->from('oil');
			$this->db->where('status_produk', 'Publish');
			$this->db->where('id_kategori', $id_kategori);
			$query = $this->db->get();
			return $query->row();
		}

		// Listing kategori_oil
		public function listing_kategori_oil()
		{
			$this->db->select('oil.*,
							users.nama,
							kategori_oil.nama_kategori,
							kategori_oil.slug_kategori,
							COUNT(gambar_oil.id_gambar) AS total_gambar');
			$this->db->from('oil');
			// JOIN
			$this->db->join('users', 'users.id_user = oil.id_user', 'left');
			$this->db->join('kategori_oil', 'kategori_oil.id_kategori = oil.id_kategori', 'left');
			$this->db->join('gambar_oil', 'gambar_oil.id_produk = oil.id_produk', 'left');
			//END JOIN
			$this->db->group_by('oil.id_kategori');
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->result();
		}

		// Detail produk
		public function detail($id_produk)
		{
			$this->db->select('*');
			$this->db->from('oil');
			$this->db->where('id_produk', $id_produk);
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->row();
		}

		// Detail gambar produk
		public function detail_gambar_oil($id_gambar)
		{
			$this->db->select('*');
			$this->db->from('gambar_oil');
			$this->db->where('id_gambar', $id_gambar);
			$this->db->order_by('id_gambar',' desc');
			$query = $this->db->get();
			return $query->row();
		}

		// Gambar
		public function gambar($id_produk)
		{
			$this->db->select('*');
			$this->db->from('gambar_oil');
			$this->db->where('id_produk', $id_produk);
			$this->db->order_by('id_gambar','desc');
			$query = $this->db->get();
			return $query->result();
		}


		//Tambah
		public function tambah($data)
		{
			$this->db->insert('oil', $data);
		}

		//Tambah Gambar
		public function tambah_gambar($data)
		{
			$this->db->insert('gambar_oil', $data);
		}


		//Edit
		public function edit($data)
		{
			$this->db->where('id_produk', $data['id_produk']);
			$this->db->update('oil', $data);
		}


		//Delete
		public function delete($data)
		{
			$this->db->where('id_produk', $data['id_produk']);
			$this->db->delete('oil', $data);
		}

		//Delete Gambar
		public function delete_gambar_oil($data)
		{
			$this->db->where('id_gambar', $data['id_gambar']);
			$this->db->delete('gambar_oil', $data);
		}


}

/* End of file Oil_model.php */
/* Location: ./application/models/Oil_model.php */