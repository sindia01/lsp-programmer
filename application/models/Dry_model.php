<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dry_model extends CI_Model {

	public function __construct()
		{
			parent::__construct();
			$this->load->database();
			
		}	

		// Listing all produk
		public function listing()
		{
			$this->db->select('dry.*,
							users.nama,
							kategori_dry.nama_kategori,
							kategori_dry.slug_kategori,
							COUNT(gambar_dry.id_gambar) AS total_gambar');
			$this->db->from('dry');
			// JOIN
			$this->db->join('users', 'users.id_user = dry.id_user', 'left');
			$this->db->join('kategori_dry', 'kategori_dry.id_kategori = dry.id_kategori', 'left');
			$this->db->join('gambar_dry', 'gambar_dry.id_produk = dry.id_produk', 'left');
			//END JOIN
			$this->db->group_by('dry.id_produk');
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->result();
		}

		// Listing all produk home
		public function home()
		{
			$this->db->select('dry.*,
							users.nama,
							kategori_dry.nama_kategori,
							kategori_dry.slug_kategori,
							COUNT(gambar_dry.id_gambar) AS total_gambar');
			$this->db->from('dry');
			// JOIN
			$this->db->join('users', 'users.id_user = dry.id_user', 'left');
			$this->db->join('kategori_dry', 'kategori_dry.id_kategori = dry.id_kategori', 'left');
			$this->db->join('gambar_dry', 'gambar_dry.id_produk = dry.id_produk', 'left');
			//END JOIN
			$this->db->where('dry.status_produk','Publish');
			$this->db->group_by('dry.id_produk');
			$this->db->order_by('id_produk',' desc');
			$this->db->limit(12);
			$query = $this->db->get();
			return $query->result();
		}

		//  read produk
		public function read($slug_produk)
		{
			$this->db->select('dry.*,
							users.nama,
							kategori_dry.nama_kategori,
							kategori_dry.slug_kategori,
							COUNT(gambar_dry.id_gambar) AS total_gambar');
			$this->db->from('dry');
			// JOIN
			$this->db->join('users', 'users.id_user = dry.id_user', 'left');
			$this->db->join('kategori_dry', 'kategori_dry.id_kategori = dry.id_kategori', 'left');
			$this->db->join('gambar_dry', 'gambar_dry.id_produk = dry.id_produk', 'left');
			//END JOIN
			$this->db->where('dry.status_produk','Publish');
			$this->db->where('dry.slug_produk', $slug_produk);
			$this->db->group_by('dry.id_produk');
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->row();
		}

		// Produk
		public function normal($limit,$start)
		{
			$this->db->select('dry.*,
							users.nama,
							kategori_dry.nama_kategori,
							kategori_dry.slug_kategori,
							COUNT(gambar_dry.id_gambar) AS total_gambar');
			$this->db->from('dry');
			// JOIN
			$this->db->join('users', 'users.id_user = dry.id_user', 'left');
			$this->db->join('kategori_dry', 'kategori_dry.id_kategori = dry.id_kategori', 'left');
			$this->db->join('gambar_dry', 'gambar_dry.id_produk = dry.id_produk', 'left');
			//END JOIN
			$this->db->where('dry.status_produk','Publish');
			$this->db->group_by('dry.id_produk');
			$this->db->order_by('id_produk',' desc');
			$this->db->limit($limit,$start);
			$query = $this->db->get();
			return $query->result();
		}

		// Total Produk
		public function total_dry()
		{
			$this->db->select('COUNT(*) AS total');
			$this->db->from('dry');
			$this->db->where('status_produk', 'Publish');
			$query = $this->db->get();
			return $query->row();
		}


		// Kategori Produk
		public function kategori_dry($id_kategori,$limit,$start)
		{
			$this->db->select('dry.*,
							users.nama,
							kategori_dry.nama_kategori,
							kategori_dry.slug_kategori,
							COUNT(gambar_dry.id_gambar) AS total_gambar');
			$this->db->from('dry');
			// JOIN
			$this->db->join('users', 'users.id_user = dry.id_user', 'left');
			$this->db->join('kategori_dry', 'kategori_dry.id_kategori = dry.id_kategori', 'left');
			$this->db->join('gambar_dry', 'gambar_dry.id_produk = dry.id_produk', 'left');
			//END JOIN
			$this->db->where('dry.status_produk','Publish');
			$this->db->where('dry.id_kategori', $id_kategori);
			$this->db->group_by('dry.id_produk');
			$this->db->order_by('id_produk',' desc');
			$this->db->limit($limit,$start);
			$query = $this->db->get();
			return $query->result();
		}

		// Total Kategori Produk
		public function total_kategori_dry($id_kategori)
		{
			$this->db->select('COUNT(*) AS total');
			$this->db->from('dry');
			$this->db->where('status_produk', 'Publish');
			$this->db->where('id_kategori', $id_kategori);
			$query = $this->db->get();
			return $query->row();
		}

		// Listing kategori_dry
		public function listing_kategori_dry()
		{
			$this->db->select('dry.*,
							users.nama,
							kategori_dry.nama_kategori,
							kategori_dry.slug_kategori,
							COUNT(gambar_dry.id_gambar) AS total_gambar');
			$this->db->from('dry');
			// JOIN
			$this->db->join('users', 'users.id_user = dry.id_user', 'left');
			$this->db->join('kategori_dry', 'kategori_dry.id_kategori = dry.id_kategori', 'left');
			$this->db->join('gambar_dry', 'gambar_dry.id_produk = dry.id_produk', 'left');
			//END JOIN
			$this->db->group_by('dry.id_kategori');
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->result();
		}

		// Detail produk
		public function detail($id_produk)
		{
			$this->db->select('*');
			$this->db->from('dry');
			$this->db->where('id_produk', $id_produk);
			$this->db->order_by('id_produk',' desc');
			$query = $this->db->get();
			return $query->row();
		}

		// Detail gambar produk
		public function detail_gambar_dry($id_gambar)
		{
			$this->db->select('*');
			$this->db->from('gambar_dry');
			$this->db->where('id_gambar', $id_gambar);
			$this->db->order_by('id_gambar',' desc');
			$query = $this->db->get();
			return $query->row();
		}

		// Gambar
		public function gambar($id_produk)
		{
			$this->db->select('*');
			$this->db->from('gambar_dry');
			$this->db->where('id_produk', $id_produk);
			$this->db->order_by('id_gambar','desc');
			$query = $this->db->get();
			return $query->result();
		}


		//Tambah
		public function tambah($data)
		{
			$this->db->insert('dry', $data);
		}

		//Tambah Gambar
		public function tambah_gambar($data)
		{
			$this->db->insert('gambar_dry', $data);
		}


		//Edit
		public function edit($data)
		{
			$this->db->where('id_produk', $data['id_produk']);
			$this->db->update('dry', $data);
		}


		//Delete
		public function delete($data)
		{
			$this->db->where('id_produk', $data['id_produk']);
			$this->db->delete('dry', $data);
		}

		//Delete Gambar
		public function delete_gambar_dry($data)
		{
			$this->db->where('id_gambar', $data['id_gambar']);
			$this->db->delete('gambar_dry', $data);
		}


}

/* End of file Dry_model.php */
/* Location: ./application/models/Dry_model.php */