
<!-- Title Page -->
<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo base_url() ?>assets/upload/gallery/kom.jpg);">
<h2 class="l-text2 t-center">
<?php echo $title ?>
</h2>
<p class="m-text13 t-center">

</section>

<!-- content page -->   
<section class="bgwhite p-t-60 p-b-25">
<div class="container">
<div class="row">
<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
				
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="<?php echo base_url() ?>assets/upload/gallery/kom.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('skin/cs') ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Combination Skin
							</a>
						</div>
					</div>

					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="<?php echo base_url() ?>assets/upload/gallery/kom.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('skin/os') ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Oily Skin
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="<?php echo base_url() ?>assets/upload/gallery/kom.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('skin/ds') ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Dry Skin
							</a>
						</div>
					</div>

					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="<?php echo base_url() ?>assets/upload/gallery/kom.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('skin/ns') ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Normal Skin
							</a>
						</div>
					</div>
				</div>
					</div>
						</div>
						
 					</div>
				</div>

		</div>
	</div>
</div>
</div>
</section>


