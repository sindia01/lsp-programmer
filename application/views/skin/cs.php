
<!-- Title Page -->
<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo base_url() ?>assets/upload/gallery/kom.jpg);">
<h2 class="l-text2 t-center">
<?php echo $title ?>
</h2>
<p class="m-text13 t-center">

</section>

<!-- content page -->
<section class="bgwhite p-t-60 p-b-25">
<div class="container">
<div class="row">
	<div class="col-md-8 col-lg-9 p-b-80">
		<div class="p-r-50 p-r-0-lg">
			<div class="p-b-40">
				<div class="blog-detail-img wrap-pic-w">
					<img src="<?php echo base_url() ?>assets/upload/skin/skin.jpg" alt="IMG-BLOG">
				</div>

				<div class="blog-detail-txt p-t-33">
					<h4 class="p-b-11 m-text24">
						Combination Skin Type
					</h4>

					<p class="p-b-25">
						

						Kulit kombinasi adalah tipe kulit yang memiliki dua atau lebih tipe kulit di kulitnya, dan biasanya ditandai dengan sebagian area kulit yang kering dan bersisik di sebagian wajah dan sebagian wajah lainnya berminyak. Banyak yang bilang mereka punya kulit berminyak, dimana pada kenyataannya punya kulit kombinasi.

					</p>

					<p class="p-b-25">
						Beberapa karakteristik kulit kombinasi adalah : <br>

						> Pori-pori yang terlihat besar dari pada normal tapi tidak terlalu terlihat <br>
						> Berminyak di area T-Zone (dahi, hidung dan dagu) dan kering di bagian pipi <br>
						> Terdapat blackhead <br>
					</p>
				</div>

				<div class="flex-m flex-w p-t-20">
					<span class="s-text20 p-r-20">
						Tags
					</span>

					<div class="wrap-tags flex-w">
						<a href="<?php echo base_url('combination') ?>" class="tag-item">
							Combination
						</a>

						<a href="<?php echo base_url('home/news') ?>" class="tag-item">
							Skin
						</a>
					</div>
				</div>
			</div>


		</div>
	</div>

	<div class="col-md-4 col-lg-3 p-b-80">
		<div class="rightbar">
			<!-- Categories -->
			<h4 class="m-text23 p-t-56 p-b-34">
				Skin Types
			</h4>

			<ul>
				<li class="p-t-6 p-b-8 bo6">
					<a href="<?php echo base_url('skin/cs') ?>" class="s-text13 p-t-5 p-b-5">
						Combination Skin
					</a>
				</li>

				<li class="p-t-6 p-b-8 bo7">
					<a href="<?php echo base_url('skin/os') ?>" class="s-text13 p-t-5 p-b-5">
						Oily Skin
					</a>
				</li>

				<li class="p-t-6 p-b-8 bo7">
					<a href="<?php echo base_url('skin/ds') ?>" class="s-text13 p-t-5 p-b-5">
						Dry Skin
					</a>
				</li>

				<li class="p-t-6 p-b-8 bo7">
					<a href="<?php echo base_url('skin/ns') ?>" class="s-text13 p-t-5 p-b-5">
						Normal Skin
					</a>
				</li>

			</ul>

		</div>
	</div>
</div>
</div>
</section>


