<!-- Banner -->
	<section class="banner bgwhite p-t-40 p-b-40">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
				
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="<?php echo base_url() ?>assets/upload/gallery/kom.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('combination') ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Combination Skin
							</a>
						</div>
					</div>

					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="<?php echo base_url() ?>assets/upload/gallery/kom.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('oil') ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Oily Skin
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="<?php echo base_url() ?>assets/upload/gallery/kom.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('dry') ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Dry Skin
							</a>
						</div>
					</div>

					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="<?php echo base_url() ?>assets/upload/gallery/kom.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('normal') ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Normal Skin
							</a>
						</div>
					</div>
				</div>


					</div>
						</div>
						
 					</div>
				</div>
			</div>
		</div>
	</section>