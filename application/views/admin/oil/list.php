<p>
		<a href="<?php echo base_url('admin/oil/tambah') ?>" class="btn btn-success btn-lg">
			<i class="fa fa-plus"></i> Tambah Baru 
		</a>
</p>

<?php
// Notifikasi
if($this->session->flashdata('sukses')) {
	echo '<p class="alert alert-success">';
	echo $this->session->flashdata('sukses');
	echo '</<div>';
}
?>

<table class="table table-bordered" id="example1">
	<thead>
		<tr>
			<th>NO</th>
			<th>GAMBAR</th>
			<th>NAMA</th>
			<th>KATEGORI</th>
			<th>MERK</th>
			<th>HARGA</th>
			<th>STATUS</th>
			<th>ACTION</th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1; foreach($oil as $oil) { ?>
		<tr>
			<td><?php echo $no ?></td>
			<td>
				<img src="<?php echo base_url('assets/upload/image/thumbs/'.$oil->gambar) ?>" class="img img-responsive img-thumbnail" width="60">
			</td>
			<td><?php echo $oil->nama_produk ?></td>
			<td><?php echo $oil->nama_kategori?></td>
			<td><?php echo $oil->merk?></td>
			<td><?php echo number_format($oil->harga,'0',',','.') ?></td>
			<td><?php echo $oil->status_produk?></td>
			<td>

				<a href="<?php echo base_url('admin/oil/gambar_oil/'.$oil->id_produk) ?> " class="btn btn-success btn-xs"><i class="fa fa-image"></i> Gambar (<?php echo $oil->total_gambar ?>) </a>


				<a href="<?php echo base_url('admin/oil/edit/'.$oil->id_produk) ?> " class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>

				<?php include('delete.php') ?>

			</td>
			
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>