<?php 
// ERROR UPLOAD  
if(isset($error)){
  echo '<p class="alert alert-warning">';
  echo $error;
  echo '</p>';
}

// Notifikasi error
echo validation_errors('<div class="alert alert-warning">','</div>');

// Form open
echo form_open_multipart(base_url('admin/normal/tambah/'),' class="form-horizontal"');

 ?>

 <div class="form-group ">
  <label  class="col-md-2 control-label">Nama Produk</label>
  <div class="col-md-5">
    <input type="text" name="nama_produk" class="form-control"  placeholder="Nama Produk" value="<?php echo set_value('nama_produk') ?>" required>
  </div>
</div>


<div class="form-group">
  <label  class="col-md-2 control-label">Kode Produk</label>
  <div class="col-md-5">
    <input type="text" name="kode_produk" class="form-control"  placeholder="Kode Produk" value="<?php echo set_value('kode_produk') ?>" required>
  </div>
</div>

 <div class="form-group">
  <label  class="col-md-2 control-label">Kategori Produk</label>
  <div class="col-md-5">
    <select name="id_kategori" class="form-control">
      <?php foreach ($kategori as $kategori ) { ?>
      <option value="<?php echo $kategori->id_kategori ?> " >
        <?php echo $kategori->nama_kategori ?> 
      </option>
    <?php } ?>
    </select>
    </select>
  </div>
</div>

<div class="form-group">
  <label  class="col-md-2 control-label">Merk Produk</label>
  <div class="col-md-5">
    <input type="text" name="merk" class="form-control"  placeholder="Merk Produk" value="<?php echo set_value('merk') ?>" required>
  </div>
</div>



<div class="form-group">
  <label  class="col-md-2 control-label">Harga Produkl</label>
  <div class="col-md-5">
    <input type="number" name="harga" class="form-control"  placeholder="Harga Normal" value="<?php echo set_value('harga') ?>" required>
  </div>
</div>

<div class="form-group">
  <label  class="col-md-2 control-label">Keterangan Produk</label>
  <div class="col-md-10">
    <textarea name="keterangan" class="form-control" placeholder="Keterangan" id="editor"><?php echo set_value('keterangan') ?></textarea>
  </div>
</div>

<div class="form-group">
  <label  class="col-md-2 control-label">Keyword (untuk SEO Google)</label>
  <div class="col-md-10">
    <textarea name="keyword" class="form-control" placeholder="Keyword (untuk SEO Google)"><?php echo set_value('keyword') ?></textarea>
  </div>
</div>

<div class="form-group">
  <label  class="col-md-2 control-label">Upload Gambar Produk</label>
  <div class="col-md-5">
    <input type="file" name="gambar" class="form-control" required="required">
  </div>
</div>

<div class="form-group">
  <label  class="col-md-2 control-label">Status Produk</label>
  <div class="col-md-5">
    <select name="status_produk"  class="form-control">
      <option value="Publish">Publikasikan</option>
      <option value="Draft">Simpan Sebagai Draft</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label  class="col-md-2 control-label"></label>
  <div class="col-md-5">
    <button class="btn btn-success btn-lg" name="submit" type="submit">
    	<i class="fa fa-save"></i> Simpan
    </button>
        <button class="btn btn-info btn-lg" name="reset" type="reset">
    	<i class="fa fa-times"></i> Reset
    </button>
  </div>
</div>

 <?php echo form_close(); ?>