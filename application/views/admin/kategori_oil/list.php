<p>
		<a href="<?php echo base_url('admin/kategori_oil/tambah') ?>" class="btn btn-success btn-lg">
			<i class="fa fa-plus"></i> Tambah Baru 
		</a>
</p>

<?php
// Notifikasi
if($this->session->flashdata('sukses')) {
	echo '<p class="alert alert-success">';
	echo $this->session->flashdata('sukses');
	echo '</div>';
}
?>

<table class="table table-bordered" id="example1">
	<thead>
		<tr>
			<th>NO</th>
			<th>NAMA</th>
			<th>SLUG</th>
			<th>URUTAN</th>
			<th>ACTION</th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1; foreach($kategori_oil as $kategori_oil) { ?>
		<tr>
			<td><?php echo $no ?></td>
			<td><?php echo $kategori_oil->nama_kategori ?></td>
			<td><?php echo $kategori_oil->slug_kategori ?></td>
			<td><?php echo $kategori_oil->urutan?></td>
			<td>
				<a href="<?php echo base_url('admin/kategori_oil/edit/'.$kategori_oil->id_kategori) ?> " class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>

				<a href="<?php echo base_url('admin/kategori_oil/delete/'.$kategori_oil->id_kategori) ?> " class="btn btn-danger btn-xs" onclick="return confirm('Yakin ingin menghapus data ini?')"><i class="fa fa-trash-o"></i> Delete</a>

			</td>
			
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>