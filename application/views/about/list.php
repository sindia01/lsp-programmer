
<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(<?php echo base_url() ?>assets/upload/gallery/kom.jpg);">
		<h2 class="l-text2 t-center">
			About
		</h2>
	</section>



	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-38">
		<div class="container">
			<div class="row">
				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
						<img src="<?php echo base_url() ?>assets/upload/gallery/kombinasi.jpg" alt="IMG-ABOUT">
					</div>
				</div>

				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Rekomendasi Skincare
					</h3>

					<p class="p-b-28">
						Pembuatan Website ini dilakukan untuk memenuhi tugas penulisan ilmiah saya, memilih untuk membuat website untuk merangkum beberapa skincare yang cocok untuk beberapa tipe kulit seperti tipe kulit kombinasi, berminyak, kering dan normal.
					</p>

				</div>
			</div>
		</div>
	</section>
