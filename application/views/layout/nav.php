<?php 
   // AMBIL DATA MENU DARI KONFIGURASI 
$nav_normal 				= $this->konfigurasi_model->nav_normal();
$nav_normal_mobile 			= $this->konfigurasi_model->nav_normal();
$nav_combination			= $this->konfigurasi_model->nav_combination();
$nav_combination_mobile		= $this->konfigurasi_model->nav_combination();
$nav_oil 	 				= $this->konfigurasi_model->nav_oil();
$nav_oil_mobile 	 		= $this->konfigurasi_model->nav_oil();
$nav_dry 				 	= $this->konfigurasi_model->nav_dry();
$nav_dry_mobile 	 		= $this->konfigurasi_model->nav_dry();
 ?>

<!-- <div class="search-product pos-relative bo4 of-hidden">
	<input class="s-text7 size6 p-l-23 p-r-50" type="text" name="keyword" placeholder="Search Products...">
	<button class="flex-c-m size5 ab-r-m color2 color0-hov trans-0-4">
		<i class="fs-12 fa fa-search" aria-hidden="true"></i>
	</button>
</div>
 -->




 
<div class="wrap_header">


<!-- Logo -->
<a href="<?php echo base_url() ?>" class="logo">
	<img src="<?php echo base_url('assets/upload/image/'.$site->logo) ?>" alt="<?php echo $site->namaweb ?> | <?php echo $site->tagline ?>">
</a>



<!-- Menu -->
<div class="wrap_menu">
	<nav class="menu">


		<ul class="main_menu">

			<!-- HOME -->

			<li>
				<a href="<?php echo base_url() ?>">Home</a>
			</li>

			<li>
				<a href="<?php echo base_url('skin') ?>">Skin Types</a>
			</li>


						<!-- MENU PRODUK BERJERAWAT -->
			<li>
				<a href="<?php echo base_url('combination') ?>">Combination Skin</a>
				<ul class="sub_menu">
					<?php foreach($nav_combination as $nav_combination) { ?>
					<li><a href="<?php echo base_url('combination/kategori_jer/'.$nav_combination->slug_kategori) ?>">
						<?php echo $nav_combination->nama_kategori ?>
						
					</a></li>
				<?php } ?>
				</ul>
			</li>

			<!-- MENU PRODUK BERJERAWAT -->
			<li>
				<a href="<?php echo base_url('oil') ?>">Oily Skin</a>
				<ul class="sub_menu">
					<?php foreach($nav_oil as $nav_oil) { ?>
					<li><a href="<?php echo base_url('oil/kategori_oil/'.$nav_oil->slug_kategori) ?>">
						<?php echo $nav_oil->nama_kategori ?>
						
					</a></li>
				<?php } ?>
				</ul>
			</li>

			<!-- MENU PRODUK BERJERAWAT -->
			<li>
				<a href="<?php echo base_url('dry') ?>">Dry Skin</a>
				<ul class="sub_menu">
					<?php foreach($nav_dry as $nav_dry) { ?>
					<li><a href="<?php echo base_url('dry/kategori_dry/'.$nav_dry->slug_kategori) ?>">
						<?php echo $nav_dry->nama_kategori ?>
						
					</a></li>
				<?php } ?>
				</ul>
			</li>



			<!-- MENU PRODUK NORMAL -->
			<li>
				<a href="<?php echo base_url('normal') ?>">Normal Skin</a>
				<ul class="sub_menu">
					<?php foreach($nav_normal as $nav_normal) { ?>
					<li><a href="<?php echo base_url('normal/kategori/'.$nav_normal->slug_kategori) ?>">
						<?php echo $nav_normal->nama_kategori ?>
						
					</a></li>
				<?php } ?>
				</ul>
			</li>

			<!--<li>
				<a href="<?php echo base_url('merk') ?>">Merk</a>
				<ul class="sub_menu">
					<li><a href="<?php echo base_url('merk/garnier') ?>">Garnier</a></li>
					<li><a href="home-02.html">Ponds</a></li>
					<li><a href="home-03.html">W</a></li>
				</ul>
			</li> -->


					<!-- Header Icon -->
		<div class="header-icons">
			<div class="header-social">
				<a href="<?php echo $site->facebook ?>" class="header-social-item fa fa-facebook"></a>
				<a href="<?php echo $site->instagram ?>" class="header-social-item fa fa-instagram"></a>
			</div>
	
			<a href="<?php echo base_url('login') ?>" class="header-wrapicon1 dis-block ">
				<img src="<?php echo base_url() ?>assets/template/images/icons/icon-header-01.png" class="header-icon1" alt="ICON"> 
			</a>
		</div>
			
		</ul>
			
		</form>
					<!-- Search -->
	<!-- <div class="pos-relative bo11 of-hidden">
		<form action="<?php echo base_url('normal') ?>" method="get" id="menu_search_form" class="menu_search_form">
		
		<input class="s-text7 size16 p-l-23 p-r-50" type="text" name="search-product" placeholder="Search">

		<button class="flex-c-m size5 ab-r-m color1 color0-hov trans-0-4" class="menu_search_button">
			<i class="fs-13 fa fa-search" aria-hidden="true"></i>
		</button>
	</div> -->
	</nav>
</div>


</div>
</div>
</div>

<!-- Header Mobile -->
<div class="wrap_header_mobile">
<!-- Logo moblie -->
<a href="<?php echo base_url() ?>" class="logo-mobile">
<img src="<?php echo base_url('assets/upload/image/'.$site->logo) ?>" alt="<?php echo $site->namaweb ?> | <?php echo $site->tagline ?>" alt="IMG-LOGO">
</a>

<!-- Button show menu -->
<div class="btn-show-menu">
<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
	<span class="hamburger-box">
		<span class="hamburger-inner"></span>
	</span>
</div>
</div>
</div>

<!-- Menu Mobile -->
<div class="wrap-side-menu" >
<nav class="side-menu">
<ul class="main-menu">

	<!-- HOME MOBILE -->
	<li class="item-menu-mobile">
		<a href="<?php echo base_url('home/index')  ?>">Home</a>
	</li>


	<!-- HOME ABOUT -->
	<li class="item-menu-mobile">
		<a href="<?php echo base_url('home/about')  ?>">About</a>
	</li>


	<li class="item-menu-mobile">
		<a href="<?php echo base_url('home/news') ?>">Skin Types</a>
	</li>

	<!-- MENU MOBILE PRODUK JERAWAT-->
	<li class="item-menu-mobile">
		<a href="<?php echo base_url('combination') ?>">Combination Skin</a>
		<ul class="sub-menu">
			<?php foreach($nav_combination_mobile as $nav_combination_mobile) { ?>
					<li><a href="<?php echo base_url('combination/kategori_jer/'.$nav_combination_mobile->slug_kategori) ?>">
						<?php echo $nav_combination_mobile->nama_kategori ?>
						</a></li>
				<?php } ?>

		</ul>
		<i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
		
	</li>


	<!-- MENU MOBILE PRODUK JERAWAT-->
	<li class="item-menu-mobile">
		<a href="<?php echo base_url('oil') ?>">Oily Skin</a>
		<ul class="sub-menu">
			<?php foreach($nav_oil_mobile as $nav_oil_mobile) { ?>
					<li><a href="<?php echo base_url('oil/kategori_oil/'.$nav_oil_mobile->slug_kategori) ?>">
						<?php echo $nav_oil_mobile->nama_kategori ?>
						</a></li>
				<?php } ?>

		</ul>
		<i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
		
	</li>


		<!-- MENU MOBILE PRODUK JERAWAT-->
	<li class="item-menu-mobile">
		<a href="<?php echo base_url('dry') ?>">Dry Skin</a>
		<ul class="sub-menu">
			<?php foreach($nav_dry_mobile as $nav_dry_mobile) { ?>
					<li><a href="<?php echo base_url('dry/kategori_dry/'.$nav_dry_mobile->slug_kategori) ?>">
						<?php echo $nav_dry_mobile->nama_kategori ?>
						</a></li>
				<?php } ?>

		</ul>
		<i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
		
	</li>


			<!-- MENU MOBILE PRODUK-->
	<li class="item-menu-mobile">
		<a href="<?php echo base_url('normal') ?>">Normal Skin</a>
		<ul class="sub-menu">
			<?php foreach($nav_normal_mobile as $nav_normal_mobile) { ?>
					<li><a href="<?php echo base_url('normal/kategori/'.$nav_normal_mobile->slug_kategori) ?>">
						<?php echo $nav_normal_mobile->nama_kategori ?>
						</a></li>
				<?php } ?>

		</ul>
		<i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
		
	</li>



		<!-- Header Icon -->
		<div class="header-icons">
			<div class="header-social">
				<a href="<?php echo $site->facebook ?>" class="header-social-item fa fa-facebook"></a>
				<a href="<?php echo $site->instagram ?>" class="header-social-item fa fa-instagram"></a>
			</div>
	
			<a href="<?php echo base_url('login') ?>" class="header-wrapicon1 dis-block ">
				<img src="<?php echo base_url() ?>assets/template/images/icons/icon-header-01.png" class="header-icon1" alt="ICON"> 
			</a>
		</div>
			
		</ul>
			
		</form>
					<!-- Search -->
	<div class="pos-relative bo11 of-hidden">
		<form action="<?php echo base_url('normal') ?>" method="get" id="menu_search_form" class="menu_search_form">
		
		<input class="s-text7 size16 p-l-23 p-r-50" type="text" name="search-product" placeholder="Search">

		<button class="flex-c-m size5 ab-r-m color1 color0-hov trans-0-4" class="menu_search_button">
			<i class="fs-13 fa fa-search" aria-hidden="true"></i>
		</button>
	</div>




	<!-- MENU KONTAK MOBILE -->
</ul>
</nav>
</div>
</header>